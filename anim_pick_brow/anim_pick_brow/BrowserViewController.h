//
//  BrowserViewController.h
//  anim_pick_brow
//
//  Created by S209e19 on 4/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowserViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *browser;

- (IBAction)html:(id)sender;
- (IBAction)loadJS:(id)sender;
- (IBAction)loadWeb:(id)sender;
- (IBAction)loadPDF:(id)sender;

@end
