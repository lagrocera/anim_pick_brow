//
//  BrowserViewController.m
//  anim_pick_brow
//
//  Created by S209e19 on 4/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "BrowserViewController.h"

@interface BrowserViewController ()

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)html:(id)sender {
    
    NSString *html = @"<h1>Hola desde HTML</h1><p style='color:red'>Esto es un Parrafo de texto</p>";
    [_browser loadHTMLString:html baseURL:nil];
    
}

- (IBAction)loadJS:(id)sender {
    NSString *script = @"alert('Hola desde js')";
    [self.browser stringByEvaluatingJavaScriptFromString:script];
    
}

- (IBAction)loadWeb:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://www.apple.com"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.browser setScalesPageToFit:NO];
    
    [self.browser loadRequest:request];
    
}

- (IBAction)loadPDF:(id)sender {
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"informe" ofType:@"pdf"];
    
    NSData *data = [[NSData alloc]initWithContentsOfFile:path];
    
    
    [self.browser loadData:data MIMEType:@"application/pdf" textEncodingName:nil baseURL:nil];
    
}


#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [_activityIndicator startAnimating];
    [_activityIndicator setHidden:NO];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [_activityIndicator stopAnimating];
    
}

@end















