//
//  PickerViewController.h
//  anim_pick_brow
//
//  Created by S209e19 on 4/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end
