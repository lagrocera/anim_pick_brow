//
//  AnimationsViewController.h
//  anim_pick_brow
//
//  Created by S209e19 on 4/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimationsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;

- (IBAction)simpleAnimation:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)callBackAnimation:(id)sender;
- (IBAction)animationWIthOptions:(id)sender;

@end
